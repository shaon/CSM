//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *shopIDLbl;
@property (weak, nonatomic) IBOutlet UILabel *domainName;
@property (weak, nonatomic) IBOutlet UILabel *shopName;
@property (weak, nonatomic) IBOutlet UILabel *managerName;

@end
