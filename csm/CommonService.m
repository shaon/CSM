//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import "CommonService.h"
#import "AppDelegate.h"

@implementation CommonService

-(void) showErrorMessage:(NSString*)message{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                                 initWithTitle:@"oops!"
                                 message:message
                                 delegate:nil
                                 cancelButtonTitle:@"Okay"
                                 otherButtonTitles:nil];
    [errorAlert show];
}

-(void)showLoadingMessage{
    UIAlertView *loadingMessage = [[UIAlertView alloc]
                                 initWithTitle:@"Loading.....!"
                                 message:@"\n\n"
                                 delegate:nil
                                 cancelButtonTitle:@"cancel"
                                 otherButtonTitles:nil];
    [loadingMessage show];

}

-(void)showSuccessMessage:(NSString*)message{
    UIAlertView *successAlert = [[UIAlertView alloc]
                                 initWithTitle:@"Success!"
                                 message:message
                                 delegate:nil
                                 cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
    [successAlert show];
}

-(void)showSuccessMessage:(NSString*)message withTitle:(NSString*)title{
    UIAlertView *successAlert = [[UIAlertView alloc]
                                 initWithTitle:title
                                 message:message
                                 delegate:nil
                                 cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
    [successAlert show];
}

-(UIActivityIndicatorView *)loading:(UIView*) UI{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator setFrame:UI.frame];
    [indicator setCenter: UI.center];
    [indicator setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.50]];
    [indicator setHidesWhenStopped:YES];
    
    [UI addSubview:indicator];
    return indicator;
}


-(id)initStringToClass:(NSString*) className{
    id initClass = [[NSClassFromString(className) alloc] init];
    return initClass;
}


-(SEL)initStringToMethod:(NSString*) methodName{    
    SEL initMethodName = NSSelectorFromString(methodName);
    return initMethodName;
}

-(NSString*)encodeAsString:(id)content{
    return [NSString stringWithFormat:@"%@",content];
}

-(NSString*)isNullLabel:(NSString*)name{   
    if ([name isEqualToString:@"<null>"]) {
        return @"N/A";
    }else{
        return name;
    }
}

-(NSString *)getConversionRate:(NSNumber*)order visitor:(NSNumber*)visitor {
    
    NSString *rate;
    double visitorLong = [visitor doubleValue];
    double orderLong = [order doubleValue];
    
    if ([order longValue])
    {
        double temp = (visitorLong/orderLong) * 100;
        rate = [NSString stringWithFormat:@"%.2lf",temp];
    }
    return rate;
}


-(NSString*)doTrimString:(NSString*)string{
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSString*)doTrimEmptyLines:(NSString*)string{
    
    for(int i=0; i<string.length; i++) {
        if([string characterAtIndex:i] != '\n')
            return string;
    }
    
    return @"";
}


-(NSString*)httpPrefic:(NSString*)url{    
    NSString *prefix = @"http://";
    url = [self doTrimString:url];    
    if ([url isEqualToString:@""]){
               return url;
    }else{        
        NSRange match;
        match = [url rangeOfString: prefix];
        if (match.length) {
            return url;
        }else{
            return [prefix stringByAppendingString:url];
        }
    }
}

-(NSString*)getFormatedDate:(NSString*)date{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[NSLocale currentLocale]];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate *newDate= [dateFormat dateFromString:date];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    return [dateFormat stringFromDate:newDate];
}

-(NSString*) getFilePath
{
    NSArray *arrayPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[arrayPath objectAtIndex:0] stringByAppendingString:@"LoginCredential.plist"];
    
    //return [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"LoginCredential.plist"];
    //return [[arrayPath objectAtIndex:0] stringByAppendingString:@"CommanderAdmin-Info.plist"];
}

-(NSString*) numberFormetterAsCurrency:(id)number{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [format stringForObjectValue:number];
}

-(NSString*) numberFormetterAsCurrencyNumber:(NSNumber*)number{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [format stringFromNumber:number];
}

@end
