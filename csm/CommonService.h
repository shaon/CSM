//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonService : NSObject
-(void) showErrorMessage:(NSString*)message;
-(void)showLoadingMessage;
-(void)showSuccessMessage:(NSString*)message;
-(void)showSuccessMessage:(NSString*)message withTitle:(NSString*)title;
-(id)initStringToClass:(NSString*) className;
-(SEL)initStringToMethod:(NSString*) methodName;
-(NSString*)encodeAsString:(NSNumber*)numbet;
-(NSString*)isNullLabel:(NSString*)name;
-(NSString*)doTrimString:(NSString*)string;
-(NSString*)doTrimEmptyLines:(NSString*)string;
-(NSString*)httpPrefic:(NSString*)url;
-(NSString*)getFormatedDate:(NSString*)date;
-(NSString*) numberFormetterAsCurrency:(id)number;
-(NSString*) numberFormetterAsCurrencyNumber:(NSNumber*)number;
-(NSString*) getFilePath;

@end
