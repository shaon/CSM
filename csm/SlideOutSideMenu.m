//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import "SlideOutSideMenu.h"
#import "SWRevealViewController.h"
#import "SlideOutSideMenuTableViewCell.h"

@implementation SlideOutSideMenu
{
    NSInteger previouslySelectedRow;
    
    NSArray *menuItems;
    NSArray *imageName;
}

@synthesize slideDelegate;

//Overridden the original method watch the addDelegate while initializing
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andDelegate:(id<SlideOutSideMenuDelegate>)delegateObject
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.slideDelegate = delegateObject;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    
    self.clearsSelectionOnViewWillAppear = NO;
    self.tableView.backgroundColor = [UIColor colorWithRed:140/255.f green:198/255.f blue:62/255.f alpha:1];
    self.tableView.separatorColor = [UIColor colorWithRed:70/255.f green:130/255.f blue:45/255.f alpha:1];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 ){
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];   //make fixed number of cell separator
    
    menuItems = [NSArray arrayWithObjects: @"Sign Out", @"Home", @"About", nil];
    
    imageName = [NSArray arrayWithObjects: @"signout", @"home", @"about", nil];
    
    previouslySelectedRow = -1;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.presentingViewController removeFromParentViewController];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SlideOutSideMenuTableViewCell *cell = (SlideOutSideMenuTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"SlideOutSideMenuTableViewCell"];
    
    if (cell==nil) {
        NSArray *nib=[[NSBundle mainBundle ] loadNibNamed:@"SlideOutSideMenuTableViewCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    
    // Remove seperator inset
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 )
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    [cell setSeparatorInset:UIEdgeInsetsZero];
    
    cell.menuImage.image = [UIImage imageNamed:[imageName objectAtIndex: indexPath.row]];
    cell.menuLabel.text = [menuItems objectAtIndex: indexPath.row];
    cell.menuLabel.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    cell.menuLabel.font = [UIFont boldSystemFontOfSize:14.f];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

/*
 //Manual code to add Footer View
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *sectionFooterView;
    UILabel *footerLabel;
    UIImageView *footerImage;
    UIButton *footerButton;
    
    UIImageView *separatorImage;
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(orientation==UIDeviceOrientationPortrait || orientation==UIDeviceOrientationPortraitUpsideDown)
    {
        sectionFooterView = [[UIView alloc] initWithFrame:
                                     CGRectMake(0, 0, tableView.frame.size.width, 240.0)];
        
        sectionFooterView.backgroundColor = [UIColor clearColor];
        
        footerLabel = [[UILabel alloc] initWithFrame:
                                CGRectMake(50, 202, sectionFooterView.frame.size.width, 24.0)];

        footerImage = [[UIImageView alloc] initWithFrame: CGRectMake(15, 200, 22, 22)];
        
        footerButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 190, tableView.frame.size.width, 50)];
        
        separatorImage = [[UIImageView alloc] initWithFrame: CGRectMake(0, 190, tableView.frame.size.width, 1)];
    }
    else
    {
        sectionFooterView = [[UIView alloc] initWithFrame:
                             CGRectMake(0, 0, tableView.frame.size.width, 40.0)];
        
        sectionFooterView.backgroundColor = self.tableView.backgroundColor;
        
        footerLabel = [[UILabel alloc] initWithFrame:
                       CGRectMake(50, 10, sectionFooterView.frame.size.width, 24.0)];
        
        footerImage = [[UIImageView alloc] initWithFrame: CGRectMake(15, 8, 22, 22)];
        
        footerButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
        
        separatorImage = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, tableView.frame.size.width, 1)];
        
        
    }
    
    //FooterLabel.backgroundColor = [UIColor clearColor];
    footerLabel.textColor = [UIColor whiteColor];
    footerLabel.textAlignment = NSTextAlignmentLeft;
    [footerLabel setFont:[UIFont systemFontOfSize:13]];
    
    [footerImage setImage: [UIImage imageNamed: @"gift"]];
    
    separatorImage.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    
    [footerButton addTarget:self action:@selector(footerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [sectionFooterView addSubview:footerButton];
    [sectionFooterView addSubview:footerImage];
    [sectionFooterView addSubview:footerLabel];
    [sectionFooterView addSubview:separatorImage];
    
    
    if (section == 0) {
            footerLabel.text = @"View Your Website";
            return sectionFooterView;
    }
    
    return sectionFooterView;
}
*/


-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.tableView reloadData];
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = self.tableView.backgroundColor;
}

#pragma mark - Table view delegate


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Setting The Custom Delegate
    if (slideDelegate != nil && [slideDelegate respondsToSelector:@selector(selectRowAtIndexPath:)]){
        [[self slideDelegate] selectRowAtIndexPath: [menuItems objectAtIndex:indexPath.row]];
    }
}

/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    SWRevealViewController *revealController = self.revealViewController;
    
    NSInteger row = indexPath.row;
    
    if ( row == previouslySelectedRow )
    {
        [revealController revealToggleAnimated:YES];
        return;
    }
    
    previouslySelectedRow = row;
    
    NSString *text = [menuItems objectAtIndex:indexPath.row];
    
    switch ( row )
    {
        case 1:
        {
            DashboardTableViewController *dashboard = [[DashboardTableViewController alloc] init];
            [revealController setFrontViewController:dashboard animated:YES];
            [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
            break;

        }
        case 2:
        {
            OrderViewController *order = [[OrderViewController alloc] init];
            [revealController setFrontViewController:order animated:YES];
            [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
            break;
        }
        case 3:
        {
            ProductViewController *product = [[ProductViewController alloc] init];
            [revealController setFrontViewController:product animated:YES];
            [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
            break;
        }
        case 4:
        {
            CustomerViewController *customer = [[CustomerViewController alloc] init];
            [revealController setFrontViewController:customer animated:YES];
            [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
            break;
        }
        case 0:
        case 5:
        {
            WebsiteViewController *labelController = [[WebsiteViewController alloc] init];
            labelController.text = text;
            
            [revealController setFrontViewController:labelController animated:YES];
            [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
            break;
        }
    }
}
*/
@end
