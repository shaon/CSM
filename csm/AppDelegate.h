//
//  AppDelegate.h
//  csm
//
//  Created by SHAON on 4/26/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

