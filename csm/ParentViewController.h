//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideOutSideMenu.h"

@interface ParentViewController : UIViewController <UIApplicationDelegate, UIGestureRecognizerDelegate,SlideOutSideMenuDelegate>

@property (weak, nonatomic) id innerClass;

@property (weak, nonatomic) IBOutlet UIView *innerView;

@end
