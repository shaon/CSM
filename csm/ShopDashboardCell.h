//
//  ShopDashboardCell.h
//  csm
//
//  Created by SHAON on 4/29/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopDashboardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *innerView;

@property (weak, nonatomic) IBOutlet UILabel *itemLbl;


@property (weak, nonatomic) IBOutlet UIImageView *indicatorImg;

@property (weak, nonatomic) IBOutlet UIImageView *phoneImg;
@end
