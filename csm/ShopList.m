//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import "ShopList.h"
#import "HeaderView.h"
#import "ShopListCell.h"
#import "ShopDashboard.h"

@interface ShopList ()
{
    NSArray *shopListArray;
}
@end

@implementation ShopList

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationController setNavigationBarHidden:YES];
    self.navigationController.navigationBar.translucent = NO;
    
    //self.clearsSelectionOnViewWillAppear = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];   //make fixed number of cell separator
    
    UINib *headerNib = [UINib nibWithNibName:@"HeaderView" bundle:nil];
    [self.tableView registerNib:headerNib forHeaderFooterViewReuseIdentifier:@"HeaderView"];
    
    //orderList = [[NSMutableArray alloc] init];
    
    [self setNavigationBar];
    [self loadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.presentingViewController removeFromParentViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setNavigationBar
{
    /*********************** Navigation Components Setting **************************/
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont systemFontOfSize: 14.0f];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"CENTRAL\nstation marketing";
    
    self.parentViewController.parentViewController.navigationItem.titleView = label;

}

-(void)loadData
{
    // Find out the path of recipes.plist (from plist file)
    
    NSString *filePath = [self getFilePathWithFileName:@"shopList"];
    
    if(![filePath isEqualToString:nil])
    {
        shopListArray = [self readFromPlist:filePath withKey:@"ShopList"];
    }
}

-(NSString *)getFilePathWithFileName: (NSString *) fileName
{
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    
    if(fileExists){
        return path;
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"File Missing!!" message:[NSString stringWithFormat:@"Filename %@ not found!!",fileName] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    return nil;
}

- (NSArray *) readFromPlist:(NSString *) filePath withKey:(NSString *) key
{
    NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    NSArray *data = [dic objectForKey:key];
    
    return data;
}

- (void) writeToPlist:(NSString *)filePath withContentsOfArray:(NSArray *) array       //doesn't work
{
    [array writeToFile:filePath atomically:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [shopListArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShopListCell *cell = (ShopListCell *) [tableView dequeueReusableCellWithIdentifier:@"ShopListCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShopListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSDictionary *data = [shopListArray objectAtIndex: indexPath.row];          //orderList is a array
    
    //NSLog(@"Order = = \n\n%@",data);
    
    [[cell domainName] setText: [data objectForKey:@"domain"]];
    [[cell shopIDLbl] setText: [data objectForKey:@"id"]];
    [[cell shopName] setText: [data objectForKey:@"shop"]];
    [[cell managerName] setText: [data objectForKey:@"manager"]];
    
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 35;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"HeaderView"];
    return sectionHeaderView;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShopDashboard *shopDashboard = [[ShopDashboard alloc] init];
    
    [shopDashboard setUrl: [[shopListArray objectAtIndex: indexPath.row] objectForKey:@"domain"]];
    
    [self displayContentController: shopDashboard];
}

- (void)displayContentController:(UIViewController *)content
{
    [self.navigationController pushViewController:content animated:YES];
}

@end
