//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import "ParentViewController.h"
#import "SWRevealViewController.h"
#import "SlideOutSideMenu.h"
#import "LoginTableView.h"
#import "AppDelegate.h"
#import "ShopList.h"

@interface ParentViewController () <SWRevealViewControllerDelegate>
{
    SWRevealViewController *revealController;
    UIPanGestureRecognizer *pan;
    //UITapGestureRecognizer *tap;
    
    BOOL flag;
}
@end

@implementation ParentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //set the text attribute of segmentedcontrol

    [self setNavigationBar];
    
    [self showInitialDashBoard];
    
    revealController = self.revealViewController;
    
    //[self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    pan = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(handleGesture)];
    [self.view addGestureRecognizer:pan];
    pan.delegate = self;
    
    flag = FALSE;
    
    [self setComponent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.presentingViewController removeFromParentViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setNavigationBar
{
    /*********************** Navigation Components Setting **************************/
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont systemFontOfSize: 14.0f];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"CENTRAL\nstation marketing";
    
    self.navigationItem.titleView = label;
    
     /*
     [self.parentViewController setTitle: @"CENTRAL\nstation marketing"];
     [self.parentViewController.navigationController.navigationBar  setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
     [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
     [UIFont systemFontOfSize: 16], NSFontAttributeName, nil]];
    */
    
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setBarTintColor: [UIColor colorWithRed:38/255.f green:42/255.f blue:53/255.f alpha:1]];
    
    /*
    //Navigaiton Bars Logo
    UIView *imgView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_only"]];
    titleImageView.frame = CGRectMake(0, 0, 35, 35);
    titleImageView.contentMode = UIViewContentModeScaleAspectFit;
    [imgView addSubview:titleImageView];
    self.parentViewController.navigationItem.titleView = imgView;
    */
    
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(openSlideMenu)];
    
    //UIBarButtonItem *settingButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting"] style:UIBarButtonItemStylePlain target:self action:@selector(showSettingMenu)];
    
    menuButton.tintColor = [UIColor whiteColor];
    //settingButton.tintColor = [UIColor whiteColor];
    
    //self.parentViewController.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:menuButton, nil];
    self.navigationItem.leftBarButtonItem = menuButton;
    //self.parentViewController.navigationItem.rightBarButtonItem = settingButton;
}

- (void)openSlideMenu {
    
    //[self dismissView];
    [revealController revealToggleAnimated:YES];
}

- (void)handleGesture
{
    //[self dismissView];
    
    [revealController revealToggleAnimated:YES];
    
    [self.view removeGestureRecognizer:pan];
    
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];      //Adding the default gesture recognizer

}

/*
- (void)showSettingMenu
{
    [self.view addGestureRecognizer:pan];
    
    if ([self.revealViewController.rearViewController isViewLoaded] && self.revealViewController.rearViewController.view.window){
        // viewController is visible
        [self openSlideMenu];
    }
    
    if(flag) {                   //If setting is already in the view area (apart from orientation)
        [self dismissView];
        return;
    }
    
    [self configureSetting];
}


- (void)configureSetting
{
    //Initializing with custom delegate
    flag = TRUE;
    
    settingTableView = [[SettingTableViewController alloc] initWithNibName:@"SettingTableViewController" bundle:nil andDelegate:self];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    CGFloat width = screenRect.size.width;
    CGFloat height = screenRect.size.height;
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(orientation==UIDeviceOrientationLandscapeLeft || orientation==UIDeviceOrientationLandscapeRight){
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0 ){
            width = screenRect.size.height;
            height = screenRect.size.width;
        }
    }
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)];
    [self.view addGestureRecognizer:tap];
    tap.delegate = self;
    
    settingTableView.view.frame = CGRectMake(width - 170, 0, 160, 180);
    
    settingTableView.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropdown"]];
    //settingTableView.tableView.backgroundView.alpha = 0.9;
    
    UIView *backView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, width, height)];
    
    backView.backgroundColor = [UIColor colorWithRed:17/255.f green:17/255.f blue:17/255.f alpha:0.5];
    
    [backView addSubview: settingTableView.view];
    
    [self.view addSubview: backView];
    
}


-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(flag)
    {
        [self dismissView];
        [self configureSetting];
    }
}

- (BOOL)gestureRecognizer:(UITapGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // test if our subview is on-screen
    if (self.view != nil) {
        if ([touch.view isDescendantOfView: settingTableView.view]) {
            // we touched our subviews surface
            return NO; // ignore the touch
        }
    }
    
    return YES; // handle the touch if its on the superview
}
 */

-(void)setComponent
{
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(orientation==UIDeviceOrientationLandscapeLeft || orientation==UIDeviceOrientationLandscapeRight)
    {
        appDel.window.frame =  CGRectMake(0, 0, appDel.window.frame.size.width+20, appDel.window.frame.size.height);
    }
    else{
        appDel.window.frame =  CGRectMake(0, 20, appDel.window.frame.size.width, appDel.window.frame.size.height-20);
    }
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self setComponent];
}

//Setting Menu Custom Delegate
-(void)selectRowAtIndexPath:(NSString *)selectedItem
{
    NSLog(@"%@",selectedItem);
    
    if([selectedItem isEqualToString:@"Sign Out"])
    {
        LoginTableView *login = [[LoginTableView alloc] init];
        
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:login];
        
        AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        
        app.window.rootViewController = nav;
        
    }
    else if([selectedItem isEqualToString:@"Home"])
    {
        ShopList *shopList = [[ShopList alloc] init];
        [self displayContentController: shopList];
    }
    else if([selectedItem isEqualToString:@"About"])
    {
        
    }
    [self openSlideMenu];
}


/***********************End of Navigation Setting**************************/

-(void) showInitialDashBoard {
    
    ShopList *shopList = [[ShopList alloc] init];
    [self displayContentController: shopList];
}

/*
- (IBAction)segmentPressed:(id)sender
{
    //[self updateSegmentedImages: [sender selectedSegmentIndex]];
    
    if([sender selectedSegmentIndex] == 0)
    {
        DashboardTableViewController *dashboard = [[DashboardTableViewController alloc] init];
        [self displayContentController:dashboard];
        
    }
    else if([sender selectedSegmentIndex] == 1)
    {
        CollectionViewLayout *collectionViewLayout=[[CollectionViewLayout alloc]init];
        
        CategoryCollectionView *category = [[CategoryCollectionView alloc] initWithCollectionViewLayout:collectionViewLayout];
        [self displayContentController:category];
    }
    else if([sender selectedSegmentIndex] == 2)
    {
        OrderListTable *orderList = [[OrderListTable alloc] init];
        [self displayContentController:orderList];

    }
    else if([sender selectedSegmentIndex] == 3)
    {
        ActiveInactiveCustomerTable *customerCountList = [[ActiveInactiveCustomerTable alloc] init];
        [self displayContentController:customerCountList];
    }
    
}
*/
- (void)displayContentController:(UIViewController *)content
{
    [self releasePreviousViews];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:content];
    
    [self addChildViewController:nav];
    nav.view.frame = [self.innerView bounds];
    [self.innerView addSubview:nav.view];
    [nav didMoveToParentViewController:self];
    
}

-(void)releasePreviousViews
{
    [[[self.innerView subviews] lastObject] removeFromSuperview];
    [[[self childViewControllers] lastObject] removeFromParentViewController];
}

@end
