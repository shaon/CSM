//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonService.h"
//#import "UserService.h"

@interface LoginTableView : UITableViewController <UITextFieldDelegate>

@property(nonatomic,strong) CommonService *commonService;
//@property(nonatomic,strong) UserService *userService;

-(void)saveLoginDetails;
-(void)loadLoginDetails;

@end
