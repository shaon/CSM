//
//  ShopDashboard.m
//  csm
//
//  Created by SHAON on 4/29/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import "ShopDashboard.h"
#import "ShopDashboardCell.h"

@interface ShopDashboard ()
{
    NSString *shopTitle;
    NSArray *cellItems;
}
@end

@implementation ShopDashboard

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //NSString *string = @"oop:ack:bork:greeble:ponies";
    shopTitle = [[NSString stringWithFormat:@"%@", [[[self url] componentsSeparatedByString: @"."] firstObject]] uppercaseString];
    
    cellItems = [[NSArray alloc] initWithObjects:@"Lead Central",@"Campaign Tracking",@"Site Traffic",@"Review Central",@"Work Log",@"Reports", nil];
    
    [self setNavigationBar];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setNavigationBar
{
    /*********************** Navigation Components Setting **************************/
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont systemFontOfSize: 14.0f];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = [NSString stringWithFormat:@"%@ Dashboard\n%@", shopTitle, [self url]];
    
    self.parentViewController.parentViewController.navigationItem.titleView = label;
    
    //Back Bar Button
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    backButton.tintColor = [UIColor whiteColor];
    self.parentViewController.parentViewController.navigationItem.rightBarButtonItem = backButton;
}

-(void)goBack
{
    self.parentViewController.parentViewController.navigationItem.rightBarButtonItem = nil;
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShopDashboardCell *cell = (ShopDashboardCell *) [tableView dequeueReusableCellWithIdentifier:@"ShopDashboardCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShopDashboardCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.itemLbl.text = [cellItems objectAtIndex: indexPath.row];
    
    cell.innerView.layer.cornerRadius = 5.0f;
    
    if(indexPath.row){
        cell.phoneImg.hidden = YES;
        cell.indicatorImg.hidden = YES;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

@end
