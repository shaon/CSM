//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//Custom Delegate
@class SlideOutSideMenu;
@protocol SlideOutSideMenuDelegate <NSObject>

@required                                               //we can write optional delegate too
-(void)selectRowAtIndexPath:(NSString *)selectedItem;
@end

@interface SlideOutSideMenu : UITableViewController <UITableViewDataSource, UITableViewDelegate>
{
    __weak id <SlideOutSideMenuDelegate> slideDelegate;
}

@property (nonatomic, weak) id <SlideOutSideMenuDelegate> slideDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andDelegate:(id<SlideOutSideMenuDelegate>)delegateObject;

@end
