//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import "LoginTableView.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import "LoginTableViewCell.h"

@interface LoginTableView ()
{
    UITextField *emailText;
    UITextField *passwordText;
    
    NSString *userID, *password;
    
    BOOL rememberStatus;
}

@end

@implementation LoginTableView

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:134/255.f green:196/255.f blue:67/255.f alpha:1];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [self loadLoginDetails];
    //rememberStatus = false;
    //[self setComponent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.presentingViewController removeFromParentViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dismissKeyboard
{
    [self.view endEditing:YES];
}


- (void)loginButtonPressed:(id)sender
{
    self.commonService = [[CommonService alloc] init];
    [self dismissKeyboard];
    
    [self performLogin];
}


-(void)performLogin
{
    if([[self.commonService doTrimString: emailText.text] isEqualToString:@""]){
        [self.commonService showErrorMessage:@"Please Enter Username"];
        [emailText becomeFirstResponder];
    }
    else if([[self.commonService doTrimString: passwordText.text] isEqualToString:@""]){
        [self.commonService showErrorMessage:@"Please Enter Password"];
        [passwordText becomeFirstResponder];
    }
    else{
        
        //If email and password is sam...
        if ([emailText.text isEqualToString: passwordText.text])
        {
            //NSLog(@"Success");
            
            [self saveLoginDetails];
            
            MainViewController *mainViewController = [[MainViewController alloc] init];
        
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:mainViewController];
            [self.view.window setRootViewController: nav];
            
        }
        else {
            [self.commonService showErrorMessage:@"Wrong email or password. Try again."];
        }
    }
}

-(void)loadLoginDetails
{
    
    CommonService *commonService = [[CommonService alloc] init];
    BOOL isExistsFile = [[NSFileManager defaultManager]fileExistsAtPath:[commonService getFilePath]];
    
    if (isExistsFile)
    {
        NSArray *loginDetails = [[NSArray alloc] initWithContentsOfFile:[commonService getFilePath]];
        
        if([[loginDetails objectAtIndex:0] isEqualToString: @"1"])
        {
            rememberStatus = true;
            userID = [loginDetails objectAtIndex:1];
            password = [loginDetails objectAtIndex:2];
        
            return;
        }
    }
    
    rememberStatus = false;
}

-(void)saveLoginDetails
{
    CommonService *commonService = [[CommonService alloc] init];
    
    if(rememberStatus)
    {
        //Save Flag = 1
        NSArray *userData = [[NSArray alloc]initWithObjects: @"1", emailText.text, passwordText.text, nil] ;
        [userData writeToFile:[commonService getFilePath] atomically:YES];
    }
    else {
        //Don't Save Flag = 0
        NSArray *userData = [[NSArray alloc]initWithObjects: @"0", nil] ;
        [userData writeToFile:[commonService getFilePath] atomically:YES];
    }
}

-(void)rememberIDSwitchAction:(UISwitch *)sender
{
    if(sender.on){
        rememberStatus = true;
        NSLog(@"ON");
    }
    else{
        rememberStatus = false;
        NSLog(@"OFF");
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == emailText)
    {
        [passwordText becomeFirstResponder];
    }
    else if (textField == passwordText){
        [self performLogin];
    }
    return YES;
}

/*
-(void)setComponent
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(orientation==UIDeviceOrientationLandscapeLeft || orientation==UIDeviceOrientationLandscapeRight){
        self.tableView.contentInset = UIEdgeInsetsMake(15, 0, 0, 0);
    }
    else{
        self.tableView.contentInset = UIEdgeInsetsMake(70, 0, 0, 0);
    }
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self setComponent];
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 568;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LoginTableViewCell *cell = (LoginTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"LoginTableViewCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LoginTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    // Configure the cell...
    
    /****************** Image LeftView ******************/
        
    UIImageView *imgEmail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
    imgEmail.image = [UIImage imageNamed:@"user"];
    imgEmail.alpha = 0.6;
    imgEmail.contentMode = UIViewContentModeScaleAspectFit;
    

    cell.emailText.leftViewMode = UITextFieldViewModeAlways;
    cell.emailText.delegate = self;
    cell.emailText.leftView = imgEmail;
    
    
    UIImageView *imgPass = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
    imgPass.image = [UIImage imageNamed:@"password"];
    imgPass.alpha = 0.6;
    imgPass.contentMode = UIViewContentModeScaleAspectFit;
    
    cell.passwordText.leftViewMode = UITextFieldViewModeAlways;
    cell.passwordText.delegate = self;
    cell.passwordText.leftView = imgPass;
    
    cell.emailText.layer.cornerRadius = 17.f;
    cell.passwordText.layer.cornerRadius = 17.f;
    cell.loginButton.layer.cornerRadius = 17.f;
    /*******************************************************/
    
    [cell.passwordText setSecureTextEntry:YES];
    cell.emailText.returnKeyType = UIReturnKeyNext;
    cell.passwordText.returnKeyType = UIReturnKeyGo;

    if(rememberStatus)
    {
        [cell.rememberUserID setOn:YES animated:YES];
        
        cell.emailText.text = userID;
        cell.passwordText.text = password;
    }
    else {
        [cell.rememberUserID setOn:NO animated:YES];
    }

    emailText = cell.emailText;
    passwordText = cell.passwordText;
    
    //Configure the position of UISwitch
    CGRect rect = CGRectMake(230, 345, 0, 0);
    cell.rememberUserID.frame = rect;
    
    cell.rememberUserID.transform = CGAffineTransformMakeScale(0.4, 0.4);
    
    //Action performed when buttons are pressed
    [cell.loginButton addTarget:self action:@selector(loginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    //Action performed when Remember User ID is on
    [cell.rememberUserID addTarget:self action:@selector(rememberIDSwitchAction:) forControlEvents:UIControlEventValueChanged];
    
    return cell;
}

@end
