//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "SlideOutSideMenu.h"
#import "ParentViewController.h"

@interface MainViewController ()<SWRevealViewControllerDelegate>

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    SWRevealViewController *mainRevealController;
    
    //for iPhone
    ParentViewController *parent = [[ParentViewController alloc] init];
    
    SlideOutSideMenu *rearViewController = [[SlideOutSideMenu alloc] initWithNibName:@"SlideOutSideMenu" bundle:nil andDelegate:parent];
    
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController: parent];
    
    mainRevealController =
        [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:navigation];
    
    
    mainRevealController.rearViewRevealWidth = 260;           //width of slideout menu
    mainRevealController.rearViewRevealOverdraw = 0;        //extended reveal
    mainRevealController.bounceBackOnOverdraw = NO;
    mainRevealController.stableDragOnOverdraw = YES;
    
    mainRevealController.delegate = self;
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    appDel.window.rootViewController = mainRevealController;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.presentingViewController removeFromParentViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
