//
//  LoginTableCell.h
//  csm
//
//  Created by SHAON on 4/27/15.
//  Copyright (c) 2015 Crusnic Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (weak, nonatomic) IBOutlet UISwitch *rememberUserID;

@end
